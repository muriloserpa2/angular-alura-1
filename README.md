# Angular Bytebank

<img src="https://angular.io/assets/images/logos/angularjs/AngularJS-Shield.svg" alt="drawing" width="100"/>

Projeto de estudo do framework Angular construído em cursos da plataforma Alura.<br>
Esse projeto foi gerado com [Angular CLI](https://github.com/angular/angular-cli) versão 12.1.4.

Cursos:
 - Começando com o framework - carga horária: 10hs

### Components
Fazer parte da base do desenvolvimento de uma aplicação Angular.
Após construídos os arquivos pertinentes ao componente (html, css, ts), ele deve ser adicionado no `declarations` de um módulo para que possa ser utilizado a través da tag definida no `seletor`do seu arquivo typescript.

### Angular instructions
#### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

#### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

#### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

#### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

#### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

#### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Developers
Murilo Serpa

[1]: https://www.linkedin.com/in/murilo-serpa-51a9a0101/
[2]: https://gitlab.com/muriloserpa2

[![linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)][1]
[![gitlab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)][2]

Kamila Serpa

[3]: https://www.linkedin.com/in/kamila-serpa/
[4]: https://gitlab.com/java-kamila

[![linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)][3]
[![gitlab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)][4]
